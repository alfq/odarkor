export enum Status {
  unapproved = "submissions", //TODO: change submissions to unapproved and update backend.
  approved = "approved",
  rejected = "rejected"
}
